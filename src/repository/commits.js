'use strict';

const gitRawCommits = require('git-raw-commits');
const toArray = require('stream-to-array');
const conventionalCommitsParser = require('conventional-commits-parser');
const semver = require('semver');

// A commit created for bumping version
function isBumpCommit(commit) {
  return !commit.type && semver.valid(commit.header);
}

function getCommitList(tag) {
  const parsedCommits = gitRawCommits({ from: tag, format: '%B%n-hash-%n%H' })
    .pipe(conventionalCommitsParser());

  return toArray(parsedCommits)
    .then((commits) => {
      if (commits.length === 1 && isBumpCommit(commits[0])) {
        return [];
      }

      return commits;
    });
}

module.exports = {
  getCommitList,
};
