/* eslint-disable no-param-reassign */
'use strict';

const test = require('ava');
const sinon = require('sinon');
const util = require('util');
const proxyquire = require('proxyquire');
require('../helpers/mockPackageJson');

let addTag;
let gitlabci;
test.beforeEach((t) => {
  t.context.CI_PROJECT_URL = process.env.CI_PROJECT_URL;
  process.env.CI_PROJECT_URL = 'https://gitlab.com/ikhemissi/dummy-project';
  t.context.CI_PROJECT_PATH = process.env.CI_PROJECT_PATH;
  process.env.CI_PROJECT_PATH = 'ikhemissi/dummy-project';

  addTag = sinon.stub().yields({ release: { details: 'release details' } });
  gitlabci = proxyquire('../../src/gitlabci', {
    gitlab: () => ({ projects: { repository: { addTag } } }),
  });
});

test.afterEach((t) => {
  process.env.CI_PROJECT_URL = t.context.CI_PROJECT_URL;
  process.env.CI_PROJECT_PATH = t.context.CI_PROJECT_PATH;
});

test('must create a release in GitLab', (t) => {
  const settings = { token: 'token', version: '1.1.0', hash: 'commithash', notes: 'release notes' };
  return gitlabci.addRelease(settings)
    .then((result) => {
      t.true(addTag.calledOnce);
      t.true(addTag.calledWith({
        id: 'ikhemissi/dummy-project',
        ref: 'commithash',
        tag_name: 'v1.1.0',
        message: 'v1.1.0',
        release_description: 'release notes',
      }));
      t.deepEqual(result, { release: { details: 'release details' } });
    });
});
