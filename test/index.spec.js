/* eslint-disable no-param-reassign */
'use strict';

const test = require('ava');
const sinon = require('sinon');
const stubAsPromised = require('./helpers/stubAsPromised');
require('./helpers/mockPackageJson');

const gitlabCiReleaser = require('../src');
const releases = require('../src/releases');
const repository = require('../src/repository');
const gitlabci = require('../src/gitlabci');
const npm = require('../src/npm');

const fakeCommits = [
  {
    type: 'chore',
    scope: null,
    subject: 'Add package.json',
    notes: [],
    references: [],
    hash: 'a058578c9185491db6b1d6d9ee995628e89bb00b',
  },
  {
    type: 'perf',
    scope: null,
    subject: 'Add benchmark',
    notes: [],
    references: [],
    hash: '93305f4bd04ea370691058288f366a474a1ab714',
  },
];

test.beforeEach((t) => {
  t.context.getLastVersion = repository.getLastVersion;
  repository.getLastVersion = stubAsPromised({ version: '1.1.0', tag: 'v1.1.0', hash: 'fakecommithash' });
  t.context.getCommitList = repository.getCommitList;
  repository.getCommitList = stubAsPromised(fakeCommits);
  t.context.createNewRelease = releases.createNewRelease;
  releases.createNewRelease = stubAsPromised({ version: '1.1.0', notes: 'release notes', hash: 'newcommithash' });
  t.context.addRelease = gitlabci.addRelease;
  gitlabci.addRelease = stubAsPromised({ releasedInGitlab: true });
  t.context.publish = npm.publish;
  npm.publish = stubAsPromised({ publishedInNpm: true });
});

test.afterEach((t) => {
  repository.getLastVersion = t.context.getLastVersion;
  repository.getCommitList = t.context.getCommitList;
  releases.createNewRelease = t.context.createNewRelease;
  gitlabci.addRelease = t.context.addRelease;
  npm.publish = t.context.publish;
});

test.serial('must get commits since last tag, prepare a release and add it to Gitlab', (t) => {
  return gitlabCiReleaser.release({ token: 'apiAccessToken' })
    .then(() => {
      t.true(repository.getLastVersion.calledOnce);
      t.true(repository.getLastVersion.calledWith());
      t.true(repository.getCommitList.calledOnce);
      t.true(repository.getCommitList.calledWith('fakecommithash'));
      t.true(releases.createNewRelease.calledOnce);
      t.true(releases.createNewRelease.calledWith({ version: '1.1.0', commits: fakeCommits, releaseType: undefined, releaseVersion: undefined }));
      t.true(gitlabci.addRelease.calledOnce);
      t.true(gitlabci.addRelease.calledWith({
        token: 'apiAccessToken',
        hash: 'newcommithash',
        notes: 'release notes',
        version: '1.1.0',
      }));
    });
});

test.serial('must get commits since last tag, prepare a release, add it to Gitlab and publish to npm registry', (t) => {
  return gitlabCiReleaser.release({ token: 'apiAccessToken', withNpm: true })
    .then(() => {
      t.true(repository.getLastVersion.calledOnce);
      t.true(repository.getLastVersion.calledWith());
      t.true(repository.getCommitList.calledOnce);
      t.true(repository.getCommitList.calledWith('fakecommithash'));
      t.true(releases.createNewRelease.calledOnce);
      t.true(releases.createNewRelease.calledWith({ version: '1.1.0', commits: fakeCommits, releaseType: undefined, releaseVersion: undefined }));
      t.true(gitlabci.addRelease.calledOnce);
      t.true(gitlabci.addRelease.calledWith({
        token: 'apiAccessToken',
        hash: 'newcommithash',
        notes: 'release notes',
        version: '1.1.0',
      }));
      t.true(npm.publish.calledOnce);
      t.true(npm.publish.calledWith());
    });
});
