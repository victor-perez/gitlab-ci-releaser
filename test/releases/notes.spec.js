/* eslint-disable no-param-reassign */
'use strict';

const test = require('ava');
const sinon = require('sinon');
const notes = require('../../src/releases/notes');

const fakeCommits = [
  {
    type: 'feat',
    subject: 'Add new feature',
    header: 'feat: Add new feature',
    body: 'New feature details',
    footer: 'Closes #1',
    notes: [],
    references: [
      {
        action: 'Closes',
        owner: null,
        repository: null,
        issue: '1',
        raw: '#1',
        prefix: '#',
      },
    ],
    hash: 'a5847cf31ef49f3c60e2f12e2fab35efa804ce5d',
  },
  {
    type: 'chore',
    subject: 'Add GitLab CI configuration',
    header: 'chore: Add .gitlab-ci.yml configuration file',
    body: null,
    footer: null,
    notes: [],
    references: [],
    hash: '3b30ecb2854e0b74789c43405f0ce29a45873115',
  },
  { type: 'docs',
    subject: 'Add contribution guide',
    header: 'docs: Add contribution guide',
    body: null,
    footer: null,
    notes: [],
    references: [],
    hash: 'dde859e8b4f29cf83fb3d1404ef692c287125171',
  },
  {
    type: 'docs',
    subject: 'Add README',
    header: 'docs: Add README',
    body: 'Readme file change details',
    footer: null,
    notes: [],
    references: [],
    hash: '405cbdf103249eb7fd4b1170e7f379272b3501cf',
  },
];

const expectedReleaseNotes =
`
# v1.1.0


## Features
* a5847cf31ef49f3c60e2f12e2fab35efa804ce5d Add new feature #1


## Build process and tools
* 3b30ecb2854e0b74789c43405f0ce29a45873115 Add GitLab CI configuration


## Documentation
* dde859e8b4f29cf83fb3d1404ef692c287125171 Add contribution guide
* 405cbdf103249eb7fd4b1170e7f379272b3501cf Add README
`;

test('must generate release notes', (t) => {
  const releaseNotes = notes.generate(fakeCommits, '1.1.0');
  t.is(releaseNotes.trim(), expectedReleaseNotes.trim());
});
